/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package lws

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var (
	testPath, _ = filepath.Abs("./log")
	value1024   = "hello5f7575632450456b625c49725d336a7e2158245a7c652b6e2252495e453254353f5b453c5e50" +
		"5d6d734c732c2a40763d4c294168347c4638535a2f7730444c215a5333534d745c353733554f5c3d6d545" +
		"e474d286e4e2849554928433379315d366726477d72454628362a6a3b442c716c6f2a544e63565c4f2827" +
		"443f662527696561405d296f56703727474a2b657d322e316b2d57443971655724555d3c4129783c6a292" +
		"2405555665e5c644f2769217176634c613c4358265634502a546350754c25384e2c786c652f5775623332" +
		"52292a48202e685e5b382d353f7a7c6d22617c692e774d69366a646b696a51294c3162334b65425067327" +
		"d74307c246221523a6a697356393c66345e687e7b763362357851764f552159695f2f7876664e54657c54" +
		"24563844492c664a4021436a6d70222b795670534370502032623b434f3a286f2f35453f2d517a50666d6" +
		"c4c29224e4673655a2c4f2f57637d43756a2e756d7d236e5c4674326d2c2c2b3e51734362246a7d697e2d" +
		"46733d5d337a376746443e6122217225727024205c2f7825687d5a52332328606963293857393b2841396" +
		"b225f73652f533f302e7359522d2a634b2e6f2b236e7a66432c6d6d7851565e385a494146433f3332573d" +
		"5225542c5c29525861703c2956215e4e24514e6b32233e2a3b3b5d406e2c6b5525426135683d563d6a5e7335786e757e47"
)

func TestLws_Write(t *testing.T) {
	value10M := make([]byte, 0, 10*1024*1024)
	for i := 0; i < 10*1024; i++ {
		value10M = append(value10M, value1024...)
	}

	l, err := Open(testPath, WithSegmentSize(1<<28), WithFilePrex("test_"), WithWriteFlag(WF_SYNCFLUSH, 0))
	require.Nil(t, err)
	rand.Seed(time.Now().Unix())

	for i := 0; i < 100; i++ {
		// data := []byte(fmt.Sprintf("hello world_%d", rand.Int()))
		// start := time.Now()
		err = l.Write(0, value10M)
		// t.Logf("write since: %v, total: %dM", time.Since(start), (i+1)*10)
		require.Nil(t, err)
	}
	fmt.Println("over")
	// err = l.Purge(PurgeWithSoftEntries(50))
	// require.Nil(t, err)
	l.Close()
	require.Nil(t, err)
}

func TestLws_Read(t *testing.T) {
	l, err := Open(testPath, WithFilePrex("test_"), WithSegmentSize(1<<28), WithReadNoCopy())
	require.Nil(t, err)
	it := l.NewLogIterator()
	defer it.Release()
	it.SkipToLast()
	if it.HasPreN(1) {
		ele := it.PreviousN(1)
		data, err := ele.Get()
		if err != nil {
			t.Log("err:", err)
		} else {
			t.Log("index:", ele.Index(), "----", string(data))
		}
	}
	l.Close()
	os.RemoveAll(testPath)
}

func TestLws_WriteFile(t *testing.T) {
	l, err := Open(testPath, WithSegmentSize(30), WithFilePrex("test_"), WithFileLimitForPurge(3))
	require.Nil(t, err)
	data := []byte("hello world@##########################################################@@")
	err = l.WriteToFile("test_file.wal", 0, data)
	require.Nil(t, err)
	l.Close()
}

func TestLws_ReadFile(t *testing.T) {
	l, err := Open(testPath, WithSegmentSize(30), WithFilePrex("test_"), WithWriteFlag(WF_SYNCFLUSH, 0), WithFileLimitForPurge(3))
	require.Nil(t, err)
	it, err := l.ReadFromFile("test_file.wal")
	require.Nil(t, err)
	for it.HasNext() {
		data, err := it.Next().Get()
		if err != nil {
			t.Log("err:", err)
		} else {
			t.Log(string(data))
		}
	}
	t.Log("over")
	l.Close()
}

func TestLws_Flush(t *testing.T) {
	l, err := Open(testPath, WithSegmentSize(30), WithFilePrex("test_"),
		WithFileLimitForPurge(6), WithEntryLimitForPurge(10))
	require.Nil(t, err)
	for i := 0; i < 5; i++ {
		data := []byte("hello world")
		err = l.Write(0, data)
		time.Sleep(2 * time.Second)
		require.Nil(t, err)
	}
	l.Flush()
	l.Close()
}

type Student struct {
	Age   int
	Name  string
	Grade int
	Class int
}

type StudentCoder struct {
}

func (sc *StudentCoder) Type() int8 {
	return 1
}

func (sc *StudentCoder) Encode(s interface{}) ([]byte, error) {
	return json.Marshal(s)
}
func (sc *StudentCoder) Decode(data []byte) (interface{}, error) {
	var (
		s Student
	)
	err := json.Unmarshal(data, &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}

func TestLws_WriteReadObj(t *testing.T) {
	l, err := Open(testPath, WithSegmentSize(30), WithFilePrex("test_"))
	require.Nil(t, err)
	err = l.RegisterCoder(&StudentCoder{})
	require.Nil(t, err)
	s := Student{
		Name:  "lucy",
		Age:   10,
		Grade: 3,
		Class: 1,
	}
	for i := 0; i < 5; i++ {
		s.Age++
		err = l.Write(1, s)
		require.Nil(t, err)
	}
	l.Flush()
	it := l.NewLogIterator()
	it.SkipToLast()
	for i := 0; it.HasPre() && i < 5; i++ {
		obj, err := it.Previous().GetObj()
		if err != nil {
			t.Log("err:", err)
		} else {
			t.Log(obj)
		}
	}

	l.Close()
	os.RemoveAll(testPath)
}

// var (
// 	benchLws *Lws
// 	benchWal *wal.Log
// )

// func init() {
// 	l, err := Open("./lws", WithSegmentSize(1<<26), WithFilePrex("test_"), WithWriteFlag(WF_SYNCWRITE, 0))
// 	if err != nil {
// 		panic(err)
// 	}
// 	benchLws = l
// 	benchWal, err = wal.Open("./wal", &wal.Options{
// 		SegmentSize: 1 << 26,
// 		LogFormat:   wal.JSON,
// 		NoSync:      true,
// 	})
// }

// func BenchmarkLws_Write(b *testing.B) {
// 	l := benchLws
// 	data := []byte("hello world")
// 	for i := 0; i < b.N; i++ {
// 		l.Write(0, data)
// 	}
// 	l.Flush()
// }

// func BenchmarkWal_Write(b *testing.B) {
// 	l := benchWal
// 	last, _ := l.LastIndex()
// 	data := []byte("hello world")
// 	for i := 0; i < b.N; i++ {
// 		last++
// 		l.Write(last, data)
// 	}
// 	l.Sync()
// }
