/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package allocate

import (
	"os"
	"testing"

	"chainmaker.org/chainmaker/lws/internal/file"
	"chainmaker.org/chainmaker/lws/platform"
	"github.com/stretchr/testify/require"
)

func TestBytesAllocator(t *testing.T) {
	allocator := NewBytesAllocator(1024)
	bytes, err := allocator.AllocAt(0, 100)
	require.Nil(t, err)
	require.Equal(t, 100, len(bytes))
	bytes, err = allocator.AllocAt(0, 1025)
	require.Nil(t, err)
	require.Equal(t, 1024, len(bytes))

	bytes, err = allocator.AllocAt(-1, 100)
	require.Error(t, err)

	bytes, err = allocator.AllocAt(1025, 100)
	require.Error(t, err)

	allocator.Resize(2048)
	bytes, err = allocator.AllocAt(1025, 100)
	require.Nil(t, err)
	require.Equal(t, 100, len(bytes))
	size := allocator.Size()
	require.Equal(t, size, 2048)

	allocator.Release()
}

func TestMmapAllocator(t *testing.T) {
	mmap_size := 1 << 12
	f, err := file.OpenFile("./map_file.wal", os.O_RDWR|os.O_CREATE, 0644)
	require.Nil(t, err)
	defer func() {
		f.Close()
		os.Remove("./map_file.wal")
	}()

	allocator, err := NewMmapAllocator(f, 0, mmap_size, platform.MAP_PROT_READ|platform.MAP_PROT_WRITE, platform.MAP_SHARED, false)
	if platform.IsWindows() {
		require.Error(t, err)
		return
	}
	require.Nil(t, err)
	buf, err := allocator.AllocAt(0, mmap_size)
	require.Nil(t, err)
	require.Equal(t, mmap_size, len(buf))

	buf, err = allocator.AllocAt(-1, mmap_size)
	require.Error(t, err)
	require.Equal(t, 0, len(buf))

	buf, err = allocator.AllocAt(0, mmap_size+1)
	require.Nil(t, err)
	require.Equal(t, mmap_size, len(buf))

	buf, err = allocator.AllocAt(int64(mmap_size), mmap_size)
	require.Error(t, err)
	require.Equal(t, 0, len(buf))

	allocator.Resize(0, mmap_size*2)
	size := allocator.Size()
	require.Equal(t, mmap_size*2, size)

	allocator.Release()

	size = allocator.Size()
	require.Equal(t, 0, size)
}
